.. Fiction-Sandbox documentation master file, created by
   sphinx-quickstart on Sat Jun 23 13:48:37 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Fiction-Sandbox's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Testdoc


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
